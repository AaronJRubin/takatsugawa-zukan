require 'rake'
require 'dimensions'

all_images = Rake::FileList.new("master-images/animals/ikimono/**/*-ichiran.jpg")


def crop_string_valid(crop_string)
    return crop_string.match(/^\d+x\d+\+\d+\+\d+$/)
end

def get_cropped_aspect_ratio(image)
  uncropped_dimensions = Dimensions.dimensions(image)
  uncropped_width = uncropped_dimensions[0].to_f
  uncropped_height = uncropped_dimensions[1].to_f
  res = uncropped_width / uncropped_height
  crop_file = image.pathmap("%X.crp") 
  if File.exists? crop_file
    crop_string = File.read(crop_file).strip
    if crop_string_valid(crop_string)
      cropped_dimensions = /(\d+)x(\d+)/.match(crop_string)
      cropped_width = cropped_dimensions[1].to_f
      cropped_height = cropped_dimensions[2].to_f
      res = cropped_width / cropped_height
    end
  end
  return res
end

def ideal_aspect_ratio(aspect_ratio)
  return [1.33333, 3.000000, 4.66666].min_by { |ratio| (aspect_ratio - ratio).abs} 
end

all_images.each do |image|
  aspect_ratio = get_cropped_aspect_ratio image
  ideal = ideal_aspect_ratio aspect_ratio
  if (ideal - aspect_ratio).abs > 0.001
    puts "image: #{image.pathmap("%f")}, current: #{aspect_ratio}, ideal: #{ideal}"
  end
end
