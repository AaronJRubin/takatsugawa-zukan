import yaml
import os
from plant import Plant
import glob

def parse_yaml_file(path):
    file = open(path, "r")
    contents = file.read()
    file.close()
    return yaml.load(contents)

plant_data = parse_yaml_file("data/manually_processed/plant_data.yaml")

plant_list = sorted([Plant(kana = kana, **fields) for kana, fields in plant_data.iteritems()], key = lambda plant: plant.kana)

plant_names = [plant.romaji for plant in plant_list]

boilerplate = """{% extends "base/shokubutsu.html" %}
{% block article %}

{% call article_section() %}
{% endcall %}

{% endblock %}"""

plant_name_set = set(plant_names)

for file in glob.glob("templates/sites/plants/shokubutsu/*.html"):
    plant_name = os.path.basename(file).replace(".html", "")
    if plant_name not in plant_name_set and "ichiran" not in plant_name:
        os.unlink(file)

for plant in plant_names:
    path = os.path.join("templates/sites/plants/shokubutsu", plant + ".html")
    if not os.path.exists(path):
        file = open(path, "w")
        file.write(boilerplate)
        file.close()


