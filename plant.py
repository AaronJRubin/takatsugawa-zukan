#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import romkan
from glob import glob
from re import search

def try_unicode(str):
    try:
        return str.decode('utf-8')
    except Exception:
        return str

class Plant:

    def __init__(self, latin = "", romaji = "", kana = "",
            display_name = "", type = "", hanabira_setsumei = "", hana_tsukikata = "", ha_tsukikata = "", ha_katachi = "", kyoshi = "", iro = "", ka = "", hanabira_kazu = [0], shokudoku = [], kaki = [], seiikubasho = [], bunpu = [], kishibe_type = None):
        kana = try_unicode(kana)
        if not romaji and kana:
            romaji = romkan.to_roma(kana).replace("n'", "nn")
        elif not kana and romaji:
            kana = romkan.to_kana(romaji)
        self.romaji = romaji
        self.kana = kana
        self.latin = latin
        #self.latin = latin if latin else ""
        self.display_name = display_name if display_name else self.kana
        self.kaki = kaki
        self.bunpu = bunpu
        #self.kaki = kaki if kaki else []
        #self.bunpu = bunpu if bunpu else []
        self.kishibe_type = kishibe_type
        self.seiikubasho = seiikubasho
        #self.seiikubasho = seiikubasho if seiikubasho else []
        self.type = type
        self.hanabira_kazu = hanabira_kazu
        self.hanabira_setsumei = hanabira_setsumei
        self.shokudoku = shokudoku
        self.hana_tsukikata = hana_tsukikata
        self.ha_tsukikata = ha_tsukikata 
        self.ha_katachi = ha_katachi
        self.iro = iro
        self.kyoshi = kyoshi
        self.ka = ka

        #self.type = type if type else ""

    def get_link(self):
        return "/shokubutsu/" + self.romaji + ".html"

    def image(self, suffix):
        return os.path.join("/images/shokubutsu", self.romaji, self.romaji + "-" + str(suffix) + ".jpg")

    def images(self, string):
        path = os.path.join("master-images/plants/shokubutsu", self.romaji, "*" + string + "*")
        files = glob(path + ".jpg") + glob(path + ".png") 
        unsorted = [file.replace("master-images/plants", "/images").replace(".png", ".jpg") for file in files if "ichiran" not in file]
        unsorted.sort(key = lambda fname: int(search(r"\d+", fname).group()))
        return unsorted

    def small_images(self):
        return self.images("small")

    def large_images(self):
        return self.images("large") 

    def gallery_images(self):
        return [image for image in self.images(self.romaji)]

    def blooms(self, month):
        return month in self.kaki

    def kisetsu(self):
        haru, natsu, aki, fuyu = (0, 0, 0, 0)
        for month in self.kaki:
            if month in (12, 1, 2):
                fuyu += 1
            if month in (3, 4, 5):
                haru += 1
            if month in (6, 7, 8):
                natsu += 1
            if month in (9, 10, 11):
                aki += 1
        counted = sorted([("fuyu", fuyu), ("haru", haru), ("natsu", natsu), ("aki", aki)], key = lambda tpl: tpl[1], reverse = True)
        return counted[0][0]


        

    def inhabits(self, basho):
        return basho in self.seiikubasho
