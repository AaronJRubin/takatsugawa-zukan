#前書き

このリポジトリの中には、CSS、jinja2テンプレート言語でのテンプレト、build過程とアセットパプラインを共通する二つのサイトのソースコードが入っている。そのうちの一つがwww.takatsugawa-zukan.appspot.comで閲覧が出来て、もう一つはwww.takatsugawa-shokubutsu-zukan.appspot.comで閲覧ができる。そのサイトはそれぞれ島根県益田市の高津川や益田川の動物と植物をテーマとしている。

#必要なソフト

サイトをbuildするためには、Python 2.7, Ruby 2.0+, Dart, NodeとImageMagickが必要で、画像をversion controlするためにはgit annexが必要だ。ちなみに、このrepositoryのremoteとしてGitLabを使用しているのは、GitLabがgit annexと互換性があるためだ。あと、Google App Engineにアップするためには、PythonのGoogle App Engine SDKが必要で、<https://cloud.google.com/appengine/docs/python/download>からインストールしないといけない。この必要なソフトが多いとはいえ、OS XやLinuxでのインストールは大変簡単のはずだ。以下でその説明を記述する。コマンドがうまくいかない場合は、前に`sudo`を置くとちゃんと発動する場合がある（しかし、それは理想的なやり方ではないので、できることならsudoが要らないようにフォルダーのpermissionを変える方がいいだろう）。

##git annex

https://git-annex.branchable.comには詳しい説明が記述してあって、OS Xでのインストールは`brew install git-annex`でできる。もちろん、git自体をあらかじめインストールする必要がある。使い方を簡単に説明すると、画像などのbinaryファイルは`git add filename`ではなくて、`git annex add filename`でversion control下に置き、`git annex sync --content`でそのファイルをremoteとlocalのrepository間で同期する。

##Python

好きなようにPython 2.7をインストールして, `pip`がインストールしてない場合は`easy_install pip`でpipをインストールして、このリポジトリのroot directoryで`pip install -r requirements.txt`を実行する。

##Ruby

Ruby 2.0+を好きなようにインストールして、`gem install bundler`でbundlerをインストールして、このリポジトリのroot directoryでbundle installを実行する。

##Dart

Homebrewを使って、以下のコマンドを実行しよう：

`brew tap dart-lang/dart`
`brew install dart`

Dart言語においては、dependencyは`pub get`を実行することでインストールすることができる。このコマンドは、plantsとanimalsのフォルダーで実行しないといけない（そのフォルダーの中のpubspec.yamlというファイルはRubyのGemfileやNodejsのpackage.jsonに当たるものだ）。

##Node

npmを好きなようにインストオールして、`npm install uglifyjs -g`を実行しよう。

##ImageMagick

`brew install imagemagick`でインストオールしよう。


#このプロジェクトの構造について

このサイトはstatically generatedサイトで、テンプレートはPythonのjinja2だけど、buildプロセスはRuby言語のRakeを使用している。[Jekyll](https://jekyllrb.com/)や[Hyde](http://hyde.github.io/)に似たようにものです。buildするときにはたくさんのファイルが作成されて、それを間違えて直接編集しないように、chmodでread-onlyにされる。サーバーへの依存がないstatic siteなので、今現在Google App Engineを使用しているものの、違うhostingにすることはそこまで難しいことではないだろう。

##テンプレート

テンプレートはtemplates/のフォルダーに入っていて、templates/sites/{site-name}のファイルはそれぞれのサイトのページで、templates/layoutsとtemplates/macrosとtemplate/includesのファイルは両サイトに利用されるファイルだ。

##データ

テンプレートに使用されるデータはdata/のフォルダーに入っていて、２種類に分けられている。それは、automatically processed（自動的に処理される）データと、manually processed（手動的な処理される）データだ。automatically processedのファイルは、yamlのmapでないといけなくて、そのmapのkeyがテンプレートファイルでの変数の名前で、valueがその変数の値となる。ここでは、manually processedとは、ある程度Pythonのコードで処理した上、generated_pages.pyにおいてテンプレートに渡さないといけないデータだ。データがPythonのObjectにすると便利な時に、manually processedというシステムが役に立つ。

##画像

画像はmaster-imagesというフォルダーにおいて、それぞれのサイトに当たるフォルダーに入る。この画像はいわゆる処理されていない、rawな画像で、RakeにおいてImageMagickで圧縮などされてその目的のサイトのフォルダーに入れられる。画像の縦横比が理想的でない場合、画像と同じフォルダーに.crpファイルを入れよう。例えば、abehaze.jpgをクロッピングするためには、abehaze.crpというファイルを作成して、中身を100x250+50+20などにしよう。そうすれば、サイトに入る画像は、幅が１００pxで、高さが２５０pxで、５０pxを右にずらされて、２０pxを下にずらされた画像になる（このファイルの中身は、ImageMagickの[geometry](http://www.imagemagick.org/script/command-line-processing.php#geometry)だ）。注意：master-imagesに入れる新しい画像は、git annexでversion control下に置く前に、ImageOptimなどで要らないデータを除いた方がいいだろう。

##CSS

SCSSとCompassが使用されている。`rake watch`を実行しておけば、sass/main.scssを編集しながらその変化がサイトに反映される。

##スクリプト

簡単なスクリプトはJavascriptで書いて、各サイトのichiran.htmlページでの検索機能はDart言語で書いた。`list.dart`でファイル名が終わるファイルは、R自動的に作成されるファイルで、手動的に編集してはいけない。ちなみに、`animallist.dart`と`plantlist.dart`はjinja2のテンプレートで作成されて、各サイトのarticlelist.dartはサイトの記事の内容をベースとして、Rakeで作成される。

#Buildの方法
すべてのRakeのタスクを見るためには、`rake -T`を実行しよう。`rake build_web`はテンプレートからページを使ったり、画像を圧縮したりするコマンドで、内容を変えたらすぐ実行されるように、`rake watch`が便利だ。`rake serve_plants`や`rake serve_animals`で自分のパソコンで実行するサーバーでサイトが閲覧できる（portがコマンドの出力に表示される）。Google App Engineようのサイトを作るためには、`rake compile`を使おう（このステップが必要なのは、Dart言語をしようしているためだ）。Google App Engineのプロジェクトの権限があれば、`rake deploy_plants`や`rake deploy_animals`でアップできる。何かを削除してもそれが尚サイトに反映される場合、`rake clean`, `rake clean_plants`, `rake clean_animals`, `rake clean_plants_nonimage`, や `rake clean_animals_nonimage`を必要しないといけない。その削除したものが画像でない限り、`nonimage`で終わるタスクの方が後のbuild_webの実行が早いので、その方にを勧める。

#Introduction

This repository contains the source code for two static sites that share CSS, jinja2 templates, and a build process and asset pipeline. One of the sites is hosted at www.takatsugawa-zukan.appspot.com, and one is hosted at www.takatsugawa-shokubutsu-zukan.appspot.com. The sites are visual encyclopedias of the animals and plants, respectively, inhabitating the areas of the Takatsu and Masuda rivers that flow through Masuda City, Shimane Prefecture, Japan.

#Dependencies

Python 2.7, Ruby 2.0+, Dart, Node, and ImageMagick, as well as various packages/libraries/gems for some of them, are necessary to build this site, and git annex is necessary to version control images (we use GitLab to host this repository because GitLab supports git annex). These are a lot of dependencies, but they should all be easy to install on OS X and Linux. Here's the basic procedure for Mac (if any of the shell commands below don't work the first time, try putting `sudo` in front of them). Linux should be similar; just use apt-get instead of Homebrew.

##git annex

Documentation is available at https://git-annex.branchable.com/, and the program is installable via Homebrew by executing `brew install git-annex`. To summarize: version-control images and other binary files with `git annex add filename` instead of `git add filename`, and use `git annex sync --content` to synchronize these large files between your local machine and the remote repository.

##Python

Get Python 2.7 installed however you want to, get pip installed with `easy_install pip` if you don't have it already, and run `pip install -r requirements.txt` in the root directory of this project.

##Ruby

Get Ruby 2.0 installed however you want to, get bundler installed with `gem install bundler`, and then run `bundle install` in the root directory of this project.

##Dart

The best way to get the Dart SDK installed and on your PATH is with Homebrew, executing the following commands:

`brew tap dart-lang/dart`
`brew install dart`

 Dart dependencies are fetched by executing `pub get`; you must do this in the "plants" and "animals" directories (the pubspec.yaml file in those directories is the Dart equivalent of package.json in Nodejs, etc.).

##Node

Get npm installed however you want to and run `npm install uglifyjs -g`.

##ImageMagick

Assuming that you have Homebrew installed, run `brew install imagemagick`.

#Architecture

These are statically generated sites, built using the Jinja2 template engine for Python, but using Ruby's Rake tool to manage the build process. If you've heard of Jekyll (the Ruby static site generator) or Hyde (the Python static site generator), that's basically the idea, though I don't use either of those technologies directly. Rake generates a lot of files when you build this site; so that you don't inadvertently edit a generated file, they are chmodded (chmoded?) to read-only upon generation.

##Templates

Templates are stored in templates/, with the files in templates/sites/{site-name} being "full" templates that go into the finished sites in a directory structure that mirrors that of the {site-name} directories, and the ones in templates/layouts, templates/macros, and template/includes being shared jinja2 layouts, macros, and includes that are accessible to both sites.

##Data

The data (represented via YAML) that goes into the templates is stored in data/, and there are two types, automatically processed and manually proccessed, in appropriately named subdirectories. Automatically processed data should consist of yaml files that are, at the top-level, maps; the keys of these maps will be the variables that you will have access to in template files, and the values will be what those variables represent. Manually processed data is data that needs to be processed through Python code in some way and then manually passed to templates in generate_pages.py; this can be useful in cases where (for example) we want the templates to have the data in the form of Python objects, rather than maps.

##Images

Images are stored in master-images, in subdirectories for each site; you will notice that these images (with maybe one or two exceptions) are neither cropped, resized, nor compressed. These images are processed by the Rakefile in the mostly appropriately named compress_images task. Image cropping can be done by adding ".crp" files to the same folder as an image; for an image named abehaze.html, the files that specifies how that image should be cropped should be called abehaze.crp. The syntax of the crop specification is an ImageMagick geometry object (if this description is confusing, look around the contents of master-images/ikimono for an example). Resizing and quality compression is handled (at this point) manually in the Rakefile (by making calls to ImageMagick) on a directory-by-directory basis; using ".rsz" files or ".qual" files would be an interesting idea, however, and it is one that I am considering. One thing to bear in mind with images is that independently of ImageMagick compression, you should be using ImageOptim or some other image optimizer on any new images that you want to add to the site, before you even add them to the repository!

##CSS

I use SCSS and Compass. Use `rake watch` as you edit sass/main.scss to see any changes that you make compiled to CSS, and thereby reflected in both sites, immediately.

##Scripting

I use Javascript for super simple scripts, and Dart for the search functionality on the ichiran.html pages in both sites. The dart files ending in _list.dart are generated programmatically and should not be edited (animal_list.dart and plant_list.dart are generated from jinja2 templates, and the article_list.dart file for both sites is generated by the Rakefile based on the contents of the articles that were produced from templates at a previous stage of the build process).

#Build Process

To build the sites for local testing, run `rake build_web`; to have `build web` run automatically upon changes to source files (including images, .crp files, templates, etc.), run `rake watch`; to view the sites in a local server, run `rake serve_plants` or `rake serve_animals` (the port that is being served from will be displayed in the terminal output); to produce something that will run on Google App Engine (and that is fully minified), run `rake compile`; to deploy to Google App Engine (assuming that you have the credentials), run `rake deploy_plants` or `rake deploy_animals`. You may occasionally need to run `rake clean`, `rake clean_plants`, `rake clean_animals`, `rake clean_plants_nonimage`, or `rake clean_animals_nonimage` if you delete a template or image, but see it still showing up on the site. Unless you were messing with images, favor the nonimage versions of these tasks because they're a lot faster (you don't have to recompress and re-resize images again on the next build).
