require 'csv'
require 'rake'
require 'yaml'
require 'moji'
require 'romkan'
require 'rubyxl'

Dir.chdir(__dir__)

def parse_kaki(kaki_string)
  #puts "Calling parse_kaki with #{kaki_string}"
  if kaki_string == nil
    return []
  end
  kaki_string = kaki_string.gsub("通年", "1〜12").gsub("春〜秋", "4〜11").gsub("夏〜秋", "6〜11").
    gsub("晩秋〜冬季紅葉", "").gsub("秋", "9〜11").gsub("（稀）", "").strip  
  if kaki_string.length == 0
    return []
  elsif kaki_string.length == 1
    return [kaki_string.to_i]
  else
    # puts kaki_string
    start, stop = kaki_string.split("〜").map(&:to_i)
    #puts start
    #puts stop
    return start.upto(stop).to_a
  end
end

#TYPE_MAP = { "tanshiyou" => "単子葉植物", "soushiyou" => "双子葉植物", "shida" => "シダ類" }
#TYPES = ["シダ類", "単子葉植物", "双子葉植物"]

def parse_worksheet(worksheet)
  #data = CSV.read(worksheet)[2..-1] # first two lines are headings
  type = worksheet.sheet_name # TYPE_MAP[worksheet.gsub(".csv", "")] 
  result = worksheet.select { |row| row and row.cells[0] and row.cells[0].value and row.cells[0].value != "標準和名" } . map { |row|
    puts row.cells[0].value
    res = {}
    #all_cells = row.cells
    #cells = row.cells.map { |cell| if cell.value.nil? or cell.value == "" then nil else cell.value.to_s end  }
    #puts cells
    cells = row.cells.map { |cell|
        value = cell ? cell.value : nil
        if value.nil? 
            value 
        elsif value.class == String
            #puts value.encoding
            value.strip.gsub("　", "").gsub("~", "〜").gsub("～", "〜")
        elsif value.class == Fixnum
            value.to_s
        else
            puts value.class
        end
    }
    res["kana"] = cells[0].gsub(/[(（].*[)）]/, "")
    #puts cells
    seiikubasho = []
    if cells[2] != nil
        seiikubasho << "沈水"
    end
    if cells[3] != nil
        seiikubasho << "浮葉"
    end
    if cells[4] != nil
        seiikubasho << "浮標"
    end
    if cells[5] != nil
        seiikubasho << "抽水"
    end
    if cells[6] != nil
        seiikubasho << "湿地・岸辺"
        if cells[6].include?("砂浜")
            kishibe_type = "砂浜"
        elsif cells[6].include?("kaigan")
            kishibe_type = "海岸"
        elsif cells[6].include?("kaiganiwaba")
            kishibe_type = "海岸岩場"
        end
        if not kishibe_type.nil?
            res["kishibe_type"] = kishibe_type
        end
    end
    res["seiikubasho"] = seiikubasho 
    kaki = parse_kaki(cells[7])
    res["kaki"] = kaki
    # skip tokki for now
    res["latin"] = cells[9]
    res["bunpu"] = if cells[10].nil? then [] else cells[10].gsub(/[()（） 　]/, "").chars.select { |char| char != "" } end
    if type != "シダ類"
        #puts "Parsing search parameters for plant of type #{type}"
        res["ha_tsukikata"] = cells[11]
        #puts "hananotsukikata = #{cells[11]}"
        res["ha_katachi"] = cells[12]
        #puts "hanokatachi = #{cells[12]}"
        res["kyoshi"] = cells[13]
        #puts "kyoshi = #{cells[13]}"
        res["hana_tsukikata"] = cells[14]
        #puts "hananotsukikata = #{cells[14]}"
        petal_description = cells[15] || ""
        res["hanabira_setsumei"] = petal_description
        #puts "petal_description = #{cells[16]}"
        petal_counts = Set.new(petal_description.scan(/\d+/).map(&:to_i))
        petal_ranges = petal_description.scan(/\d+〜\d+/)
        petal_ranges.each do |petal_range|
            #puts "Parsing petal range: #{petal_range}"
            beginning, ending = petal_range.split("〜").map(&:to_i)
            ((beginning + 1)...ending).each do |quantity|
                petal_counts.add(quantity)
            end
        end
        res["hanabira_kazu"] = petal_counts.to_a.sort
        #puts "petal_counts = #{res["petal_counts"]}"
        res["iro"] = cells[16]
        #puts "color_description = #{cells[16]}"
        #res["colors"] = cells[16].split(/[・、～/
        #puts "shokudoku = #{cells[17]
        shokudoku_description = cells[17] || "不明"
        res["shokudoku"] = shokudoku_description.gsub("・近縁種は毒", "").gsub("・沖縄「ンジャナ」", "").split("・")
    end
    res["ka"] = cells[18]
    res["type"] = type
    res
  }  
  return result
end

TYPES = ["単子葉植物", "双子葉植物"]
#TYPES = ["シダ類", "単子葉植物", "双子葉植物"]
workbook = RubyXL::Parser.parse("plant_data.xlsx")
worksheets = TYPES.map { |index| workbook[index] }
#puts worksheets.map(&:sheet_name)
result = worksheets.map { |worksheet| parse_worksheet(worksheet) } . flatten

#Rake::FileList.new("*.csv").map { |file| parse_worksheet(file) } . flatten

result_map = {}

result.each do |plant|
    katakana = plant["kana"]
    hiragana = Moji.kata_to_hira(katakana)
    romaji = hiragana.to_roma.gsub("n'", "nn")
    plant.delete("kana")
    result_map[romaji] = plant
end

output_file = File.open("plant_data.yaml", "w")

output_file.write(YAML.dump(result_map))
